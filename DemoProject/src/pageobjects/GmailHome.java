package pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="GmailHome"                                
     , summary=""
     , relativeUrl=""
     , connection="UICon"
     )             
public class GmailHome {
	WebDriver driver;
	public GmailHome(WebDriver driver) {
		this.driver = driver;
	}
	@LinkType()
	@FindBy(xpath = "//div[@class='header__aside']//span[@class='laptop-desktop-only']")
	public WebElement btn_CreateAnAccount;
	@TextType()
	@FindBy(xpath = "//input[@name='firstName']")
	public WebElement text_FirstName;
	
	public void clickOnNextBtn(){
		driver.findElement(By.xpath("(//span[@class='VfPpkd-vQzf8d'])[1]")).click();
	}
			
}
