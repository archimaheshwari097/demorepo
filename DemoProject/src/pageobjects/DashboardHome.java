package pageobjects;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.provar.core.testapi.annotations.*;

@Page( title="DashboardHome"                                
     , summary=""
     , relativeUrl=""
     , connection="DemoConnection"
     )             
public class DashboardHome {

	@LinkType()
	@FindBy(xpath = "//div[contains(@class,'active') and contains(@class,'oneContent')]//a[normalize-space(.)='New Dashboard']")
	public WebElement newDashboard;

	@PageFrame()
	public static class Frame {

		@TextType()
		@FindBy(xpath = "//input[@id='dashboardNameInput']")
		public WebElement name;
		@ButtonType()
		@FindBy(xpath = "//button[@id='submitBtn']")
		public WebElement create;
	}

	@FindBy(xpath = "//iframe")
	public Frame frame;
			
}
